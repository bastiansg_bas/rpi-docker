FROM registry.gitlab.com/bastiansg_bas/rpi-docker/rpi-python-37:1.0.0

# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    git zsh libffi-dev -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp

# install pip jupyter requirements
COPY jupyter-requirements.txt .
RUN pip install -r jupyter-requirements.txt

# zsh as default
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
ENV SHELL=/usr/bin/zsh

# therminal dark theme
RUN mkdir -p ~/.jupyter/lab/user-settings/@jupyterlab/terminal-extension \
    && echo '{"theme": "dark"}' > ~/.jupyter/lab/user-settings/@jupyterlab/terminal-extension/plugin.jupyterlab-settings

# clean
RUN rm -rf /tmp/*

WORKDIR /root
