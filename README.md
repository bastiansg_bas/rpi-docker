# RPI Docker

_Set of useful Docker images for the raspberry pi (64 bit version)_

Setup
----

```bash
$ git clone https://gitlab.com/bastiansg_bas/rpi-docker
```

This repo requires Docker 20.10 or above

Usage
-----

You can do _make build_, _make run_ and _make push_ with the required env **IMAGE_NAME**

### Examples

##### _rpi-base_ build example
```bash
$ make build IMAGE_NAME=rpi-base
```
##### _sense-hat_ run example
```bash
$ make run IMAGE_NAME=sense-hat
```
##### _sense-hat-jupyter_ push example
```bash
$ make push IMAGE_NAME=sense-hat-jupyter
```
