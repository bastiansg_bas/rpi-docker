REPO = registry.gitlab.com
IMAGE = bastiansg_bas/rpi-docker/$(IMAGE_NAME)
VERSION = 1.0.0

login: check-image-name
	docker login $(REPO)

build: check-image-name
	docker build -t $(REPO)/$(IMAGE):$(VERSION) -f ./$(IMAGE_NAME)/Dockerfile .
	docker tag $(REPO)/$(IMAGE):$(VERSION) $(REPO)/$(IMAGE):latest

run: check-image-name
	docker run -it --rm --privileged --network host $(REPO)/$(IMAGE):$(VERSION)

push: check-image-name build login
	docker push $(REPO)/$(IMAGE):$(VERSION)
	docker push $(REPO)/$(IMAGE):latest

check-image-name:
ifndef IMAGE_NAME
	$(error parameter IMAGE_NAME is required)
endif
